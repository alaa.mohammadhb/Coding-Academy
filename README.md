![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Orange_logo.svg/200px-Orange_logo.svg.png "Logo Title Text 1")
# Coding Academy by Orange™
> _Yaser Muhammed Jaser Saleh_


**Description :**
This repository holds the solutions submitted in partial fulfillment of the participation requirements of the Coding Academy .

**Subjects :**

| Subject          | Provided      | Comments  |
| :-------------   |:-------------:|:-----:|
| HTML Basic       | ✔️            |  |
| HTML Advanced    | ✔️            |  |
| CSS Positioning  | ✔️            |  |
| CSS Props        | ✔️            |  |
| CSS Selectors    | ✔️            |  |
| CSS Flexbox      | ✔️            |  |
| CSS Grid         | ✔️            |  |
| Simple Bootstrap | ✔️            |  |
| Adv Bootstrap    | ✔️            |  |
| Project One      | ✔️            |  |
| JS Basic         | ✔️            |  |
| JS Arrays        | ✔️            |  |
| JS Loops         | ✔️            |  |
| JS While         | ✔️            |  |
| JS Conditions    | ✔️            |  |
| JS Functions     | ✔️            |  |
| JS Recursion     | ❌            |  |
| JS forEach       | ✔️            |  |
| JS Reduce&Filter | ✔️            |  |
| JS Challenge     | ❌            |  |
| JS Objects       | ✔️            |  |
| JS DOM           | ✔️            |  |
| jQuery Basic#1   | ✔️            |  |
| JQuery Advanced  | ✔️            |Replaced due to laptop format  |
| Project Two      | ✔️            |  |
| JS OOJS          | ✔️            |  |
| JS ES6           | ✔️            | Part 2 is not done yet |
|React-mini-apps   | ❌            |  |
|Project three     | ❌            |  |
